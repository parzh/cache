import "mocha";
import { expect } from "chai";

import { Cache, Thing } from "./helpers";

describe("#constructor", () => {
	it("creates instance of Cache", () => {
		expect(() => new Cache()).to.not.throw();
	});

	it("accepts the same arguments as WeakMap constructor", () => {
		let cache: Cache<Thing> | null = null;
		const thing = <Thing> { };

		expect(() => cache = new Cache([
			[
				// cacheable
				thing,

				// cache
				{ number: { value: 42, valid: true } },
			],
		])).to.not.throw();

		expect(cache!.getValue(thing, "number")).to.equal(42);
	});
});
