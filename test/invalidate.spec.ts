import "mocha";
import { expect } from "chai";

import { Cache, Thing } from "./helpers";

describe("#invalidate", () => {
	const thing = <Thing> {};
	const cache = new Cache<Thing>();

	// ***

	beforeEach(() => {
		cache.setValue(thing, "number", 42);
		cache.setValue(thing, "string", "hello");
	});

	it("invalidates cache for value", () => {
		cache.invalidate(thing, "number");

		expect(cache.getValue(thing, "number")).to.be.undefined;
	});

	it("invalidates multiple cache entries", () => {
		cache.invalidate(thing, [ "number", "string" ]);

		expect(cache.getValue(thing, "number")).to.be.undefined;
		expect(cache.getValue(thing, "string")).to.be.undefined;
	});
});
