import "mocha";
import { expect } from "chai";

import { Cache, Thing } from "./helpers";

describe("#setValue", () => {
	const cache = new Cache<Thing>();
	const thing = <Thing> {};

	// ***

	it("unconditionally writes value in cache", () => {
		let value: number | void = void 0;

		expect(() => cache.setValue(thing, "number", 42)).to.not.throw();
		expect(() => value = cache.get(thing)!.number!.value).to.not.throw(TypeError);
		expect(value).to.equal(42);
	});
});
