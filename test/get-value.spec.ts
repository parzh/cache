import "mocha";
import { expect } from "chai";

import { Cache, Thing } from "./helpers";

describe("#getValue", () => {
	const thing = <Thing> {};
	const cache = new Cache<Thing>();

	// ***

	it("unconditionally returns cached value", () => {
		expect(cache.getValue(thing, "number")).to.be.undefined;
	});

	it("returns cached value if is it valid, otherwise initializes it", () => {
		let recalculated: boolean | null = null;

		const getValue = () => cache.getValue(thing, "number", () => ((recalculated = true), 42));

		let value = getValue();

		expect(value).to.equal(42);
		expect(recalculated).to.be.true;

		recalculated = false;
		value = getValue();

		expect(value).to.equal(42);
		expect(recalculated).to.be.false;
	});
});
