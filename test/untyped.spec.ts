import "mocha";
import { expect } from "chai";

import { Cache, Thing } from "./helpers";

describe(".untyped", () => {
	const thing = <Thing> {};
	const cache = new Cache<Thing>().untyped;

	// ***

	it("removes any key ownership constraints from the cache object", () => {
		const key = Math.random().toString();
		const val = "whatever";

		cache.setValue(thing, key, val);

		expect(cache.getValue(thing, key)).to.equal(val);
	});
});
