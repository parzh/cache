export * from "../src";

export interface Thing {
	readonly number: number;
	readonly string: string;
}
