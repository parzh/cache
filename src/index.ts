/** @private */
interface Entry<Value> {
	valid: boolean;
	value: Value | void;
}

/** @private */
type Entries<Cacheable extends object> = {
	[Key in keyof Cacheable]?: Entry<Cacheable[Key]>;
};

export class Cache<
	Cacheable extends object,
	__Key extends keyof Cacheable = keyof Cacheable,
> extends WeakMap<Cacheable, Entries<Cacheable>> {
	/**
	 * Peel off all key ownership constraints.  
	 * __Use for edge cases and/or as the last resort.__
	 * @example
	 * cache.untyped.setValue(thing, "customKey", 42);
	 */
	get untyped(): Cache<Record<keyof any, any>> {
		return this;
	}

	/**
	 * Invalidate cached value
	 * @param obj Cacheable object
	 * @param key Key of cacheable object
	 */
	invalidate(obj: Cacheable, key: __Key): void;

	/**
	 * Invalidate several cached values simultaneously
	 * @param obj Cacheable object
	 * @param keys Array of keys of cacheable object
	 */
	invalidate(obj: Cacheable, keys: __Key[]): void;

	/**
	 * Invalidate cached value
	 * @param obj Cacheable object
	 * @param key Key of cacheable object
	 */
	invalidate(obj: Cacheable, keys: __Key | __Key[]): void {
		const _keys = ([] as __Key[]).concat(keys);

		for (const key of _keys)
			this._invalidate(obj, key);
	}

	/**
	 * Store value in cache
	 * @param obj Cacheable object
	 * @param key Key of the cacheable object
	 * @param value Value to be stored in cache
	 */
	setValue<Key extends __Key>(obj: Cacheable, key: Key, value: Cacheable[Key]): void {
		this._setEntry(obj, key, { value, valid: true });
	}

	/**
	 * Get value from cache
	 * @param obj Cacheable object
	 * @param key Key of cacheable object
	 */
	getValue(obj: Cacheable, key: __Key): Cacheable[__Key] | void;

	/**
	 * Get value from cache.  
	 * If cache is invalid, it will be initialized (see description for `init` parameter).
	 * @param obj Cacheable object
	 * @param key Key of cacheable object
	 * @param init Initializer for non-existent, undefined or invalid values
	 */
	getValue<Key extends __Key>(obj: Cacheable, key: Key, init: () => Cacheable[Key]): Cacheable[Key];

	getValue(obj: Cacheable, key: __Key, init?: () => Cacheable[__Key]) {
		const entry = this._getEntry(obj, key);

		if (entry && entry.valid)
			return entry.value;

		if (init == null)
			return;

		const value = init();

		this.setValue(obj, key, value);

		return value;
	}

	/** @override */
	get(obj: Cacheable): Entries<Cacheable> {
		return super.get(obj) || ({} as object);
	}

	// ***

	private _getEntry<Key extends __Key>(obj: Cacheable, key: Key): Entry<Cacheable[Key]> | void {
		return this.get(obj)[key];
	}

	private _setEntry<Key extends __Key>(obj: Cacheable, key: Key, entry: Entry<Cacheable[Key]>) {
		this.set(obj, Object.assign(this.get(obj), { [key]: entry }));
	}

	private _invalidate(obj: Cacheable, key: __Key): void {
		const entry = this._getEntry(obj, key);

		if (entry) {
			entry.valid = false;
			entry.value = void 0;
		}
	}
}
